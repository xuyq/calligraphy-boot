package com.xu.calligraphy.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@MapperScan("com.xu.calligraphy.boot.dal.mapper")
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public static final int cpuNum = Runtime.getRuntime().availableProcessors();

    @Bean
    public TaskExecutor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        System.out.println(String.format("cpuNum=%s", cpuNum));
        executor.setCorePoolSize(cpuNum * 2);
        executor.setMaxPoolSize(cpuNum * 10);
        executor.setQueueCapacity(cpuNum * 5);
        executor.setKeepAliveSeconds(60);
        executor.setThreadNamePrefix("TaskThread-");

        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是由调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }
}
