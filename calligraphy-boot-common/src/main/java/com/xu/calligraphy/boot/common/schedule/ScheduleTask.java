package com.xu.calligraphy.boot.common.schedule;

import com.alibaba.fastjson.JSONObject;
import com.xu.calligraphy.boot.common.util.DateUtil;
import com.xu.calligraphy.boot.common.util.DingDingRobotUtil;
import com.xu.calligraphy.boot.common.util.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * @author xu
 * @date 2021/7/28 16:33
 */
@Configuration
@EnableScheduling
public class ScheduleTask {
    private static Logger logger = LoggerFactory.getLogger(ScheduleTask.class);

    @Value("${hangzhou-weather}")
    private String hangzhouWeatherUrl;

    @Autowired
    private HttpUtil httpUtil;

    @Autowired
    private DingDingRobotUtil dingDingRobotUtil;

    //Cron表达式范例：
    //每隔5秒执行一次：*/5 * * * * ?
    //每隔1分钟执行一次：0 */1 * * * ?
    //每天23点执行一次：0 0 23 * * ?
    //每天凌晨1点执行一次：0 0 1 * * ?
    //每月1号凌晨1点执行一次：0 0 1 1 * ?
    //每月最后一天23点执行一次：0 0 23 L * ?
    //每周星期天凌晨1点实行一次：0 0 1 ? * L
    //在26分、29分、33分执行一次：0 26,29,33 * * * ?
    //每天的0点、13点、18点、21点都执行一次：0 0 0,13,18,21 * * ?
    //在线生成 http://www.pdtools.net/tools/becron.jsp

    @Scheduled(cron = "0 0 10 * * ?")
//    @PostConstruct
    public void scheduleHangzhouWeatherUrl() {
        logger.debug("--- scheduleHangzhouWeatherUrl begin {}  thread {} ---", DateUtil.SIMPLE_DATE_FORMAT.format(new Date()), Thread.currentThread().getName());
        String result = httpUtil.sendGet(hangzhouWeatherUrl, "");
        logger.info("scheduleHangzhouWeatherUrl result={}", result);
        if (!StringUtils.isEmpty(result)) {
            try {
                JSONObject jsonObject = JSONObject.parseObject(result);
                JSONObject object = jsonObject.getJSONArray("lives").getJSONObject(0);
                String text = String.format(" **天气预报** \n 当前城市：%s\n 实时天气：%s\n 实时气温：%s\n 风力级别：%s\n 发布时间：%s\n 当前时间：%s\n ",
                        object.getString("province") + object.getString("city"), object.getString("weather"),
                        object.getString("temperature") + "摄氏度",
                        object.getString("winddirection") + "风" + object.getString("windpower") + "级",
                        object.getString("reporttime"), DateUtil.SIMPLE_DATE_FORMAT.format(new Date()));
                System.out.println(text);
                dingDingRobotUtil.sendDingDingRobotMessage(text);
            } catch (Exception e) {
                logger.error("scheduleHangzhouWeatherUrl Exception", e);
            }
        }
        logger.debug("--- scheduleHangzhouWeatherUrl end  {} ---", DateUtil.SIMPLE_DATE_FORMAT.format(new Date()));
    }
}
