package com.xu.calligraphy.boot.service;

import com.xu.calligraphy.boot.common.Result;
import com.xu.calligraphy.boot.dal.params.ActivityPublishParams;
import com.xu.calligraphy.boot.dal.query.UserQuery;

public interface UserService {
    String queryLoginName(Long id);

    Result selectById(Long id);

    Result queryUserList(UserQuery query);

    /**
     * 发送钉钉消息
     * @param params
     * @return
     */
    Result sendDingMsg(ActivityPublishParams params);
}
